;;; Fibonacci --- Summary

;;; Commentary:

;;; Code:

(defun fib (n)
  (cond
   ((= n 0) 0)
   ((= n 1) 1)
   (t (+ (fib (- n 1)) (fib (- n 2))))
   )
)

(defun fib2 (n)
  (if (< n 2)
      n
    (+ (fib (- n 1)) (fib (- n 2)))
    )
)

(defun fib-helper (n a b)
  (if (= n 0)
      a
    (fib-helper (- n 1) b (+ a b))
    )
)

(defun fib3 (n)
  (fib-helper n 0 1))

(ert-deftest my-test ()
  (should (equal (fib 0) 0))
  (should (equal (fib 1) 1))
  (should (equal (fib 2) 1))
  (should (equal (fib 3) 2))
  (should (equal (fib 4) 3))
)

(defmacro measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (message "%.06f" (float-time (time-since time)))))

;; (measure-time (fib 25))  ;; 0.14s
;; (measure-time (fib2 25)) ;; 0.13s
;; (measure-time (fib3 25)) ;; 0.000029s

(provide '0-fibonacci)
