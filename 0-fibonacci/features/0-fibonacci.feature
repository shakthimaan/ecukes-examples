Feature: Fibonacci
  In order to demonstrate ecukes 
  As an ILUGC member
  I want to implement the Fibonacci function

  Scenario: Five numbers
    Given a simple fib definition
    And given the first five numbers:
         | Input | Output |
         |     0 |      0 |
         |     1 |      1 |
         |     2 |      1 |
         |     3 |      2 |
         |     4 |      3 |

  Scenario: Five numbers
    Given a simplified fib2 definition
    And given five numbers:
         | Input | Output |
         |     0 |      0 |
         |     1 |      1 |
         |     2 |      1 |
         |     3 |      2 |
         |     4 |      3 |

  Scenario: Five numbers
    Given a tail-recursive fib3 definition
    And with the same five numbers:
         | Input | Output |
         |     0 |      0 |
         |     1 |      1 |
         |     2 |      1 |
         |     3 |      2 |
         |     4 |      3 |

