;;; 0-fibonacci-steps.el --- Summary

;;; Commentary:
;;; This file contains your project specific step definitions. All
;;; files in this directory whose names end with "-steps.el" will be
;;; loaded automatically by Ecukes.

;;; Code:

(Given "^a simple fib definition$"
  (lambda ()
    (should (equal (fboundp 'fib) t))
    ))

(And "^given the first five numbers:$"
  (lambda (arg)
    (mapcar 'fib1-check (cdr arg)) ; (("0" "0") ("1" "1") ("2" "1"))
    ))


(Given "^a simplified fib2 definition$"
  (lambda ()
    (should (equal (fboundp 'fib2) t))
    ))

(And "^given five numbers:$"
  (lambda (arg)
    (mapcar 'fib2-check (cdr arg)) ; (("0" "0") ("1" "1") ("2" "1"))
    ))

(Given "^a tail-recursive fib3 definition$"
  (lambda ()
    (should (equal (fboundp 'fib3) t))
    ))

(And "^with the same five numbers:$"
  (lambda (arg)
    (mapcar 'fib3-check (cdr arg)) ; (("0" "0") ("1" "1") ("2" "1"))
    ))
