(require 'f)

(defvar 0-fibonacci-support-path
  (f-dirname load-file-name))

(defvar 0-fibonacci-features-path
  (f-parent 0-fibonacci-support-path))

(defvar 0-fibonacci-root-path
  (f-parent 0-fibonacci-features-path))

(add-to-list 'load-path 0-fibonacci-root-path)

(require '0-fibonacci)
(require 'espuds)
(require 'ert)

(Setup
 ;; Before anything has run
 )

(Before
 ;; Before each scenario is run
 )

(After
 ;; After each scenario is run
 )

(Teardown
 ;; After when everything has been run
 )

(defalias 'fib1-check (apply-partially 'fib-check 'fib))
(defalias 'fib2-check (apply-partially 'fib-check 'fib2))
(defalias 'fib3-check (apply-partially 'fib-check 'fib3))

(defun fib-check (func list)
  (let ((input (string-to-number (car list)))
        (output (string-to-number (cadr list))))
    (should (equal (funcall func input) output))
    ))
