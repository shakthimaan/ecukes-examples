;;; Multiple of 3 and 5 --- Summary

;;; Commentary:

;;; Code:

(defun sum-3-5-slow (limit)
  (loop for x below limit
        when (or (zerop (% x 3)) (zerop (% x 5)))
        sum x))

;; https://en.wikipedia.org/wiki/Triangular_number
(defun triangular (n)
  (truncate (* n (1+ n)) 2))

(triangular 0)  ; 0
(triangular 1)  ; 1
(triangular 2)  ; 3
(triangular 3)  ; 6 (3^1, 3^2, 3^3) 1+2+3 = 6
(triangular 5)  ; 15
(triangular 6)  ; 21

(triangular 10) ; 55

(truncate 9 15) ; 0

(defun sum-3-5-fast (limit)
  (cl-flet ((triangular (n) (truncate (* n (1+ n)) 2)))
    (let ((n (1- limit)))  ; Sum multiples *below* the limit
      (- (+ (* 3 (triangular (truncate n 3))) 
            (* 5 (triangular (truncate n 5)))) 
         (* 15 (triangular (truncate n 15)))))))

(sum-3-5-fast 5) ; 3
(sum-3-5-fast 10) ; 23
(sum-3-5-fast 20) ; 78
(sum-3-5-fast 1000) ; 233168
(sum-3-5-fast 100000000) ; 2333333316666668

;; Source: http://rosettacode.org/wiki/Sum_multiples_of_3_and_5#Common_Lisp

(provide '1-multiple-3-and-5)
